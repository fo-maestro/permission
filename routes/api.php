<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'api', 'middleware' => 'api', 'as' => 'api.'], function () {
    Route::post('login', 'Onesla\Permission\Http\Controllers\AuthController@apiLogin')->name('login');
    Route::post('register', 'Onesla\Permission\Http\Controllers\AuthController@apiRegister')->name('register');
});