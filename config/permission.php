<?php

return [
    'validator' => [
        'register' => [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ],
        'login' => [
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]
    ]
];