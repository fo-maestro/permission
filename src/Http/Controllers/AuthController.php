<?php


namespace Onesla\Permission\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AuthController
{
    public function apiLogin(Request $request)
    {
        if (file_exists(config_path('permission-php'))) {
            $validator_config = include config_path('permission.php');
        } else {
            $validator_config = include __DIR__.'/../../../config/permission.php';
        }

        if (array_key_exists('validator', $validator_config)) {
            $validator = Validator::make($request->all(), $validator_config['validator']);

            if ($validator->fails()) {
                return Response::json($validator->errors());
            }
        }

        if (Auth::check()) {
            Response::json([
                'token' => Auth::user()->createToken('App')->accessToken
            ]);
        }

        if (Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ])) {
            Response::json([
                'token' => Auth::user()->createToken('App')->accessToken
            ]);
        } else {
            Response::json(['authentication' => 'Incorrect Email or Password']);
        }
    }

    public function apiRegister(Request $request)
    {
        return Response::json([
            'token' => $this->createUser($request)->createToken('App')->accessToken
        ]);
    }

    public function login(Request $request)
    {
        if (config_path('permission-php')) {
            $validator_config = config_path('permission.php');
        } else {
            $validator_config = include __DIR__.'/../../../config/permission.php';
        }

        if (array_key_exists('validator', $validator_config)) {
            $validator = Validator::make($request->all(), $validator_config['validator']);

            if ($validator->fails()) {
                Redirect::back()->withErrors($validator->errors());
            }
        }

        if (Auth::check()) {
            Redirect::to('/');
        }

        if (Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ], $request->input('remember', false))) {
            Redirect::to('/');
        } else {
            Redirect::back()->withErrors(['authentication' => 'Incorrect Email or Password']);
        }
    }

    public function logout()
    {
        Auth::logout();
        Redirect::to('/login');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), config('permission.validator.register'));

        if ($validator->fails()) {
            Redirect::back()->withErrors($validator->errors());
        }

        $this->createUser($request);

        return Redirect::to('/login');
    }

    private function createUser(Request $request)
    {
        $user = $request->all();
        $user['password'] = Hash::make($request->input('password'));

        return DB::table('users')->insert($user);
    }
}